<?php
namespace chronicle\auth;
use phpseclib\Crypt\RSA;

class V1 {

  public function generate_nonce() {
    return base64_encode(random_bytes(32));
  }

  public function create_rsa_sha512_signature(
      $rsa,
      $method,
      $url,
      $query,
      $username,
      $body,
      $nonce,
      $timestamp) {
    $base = $this->build_string(
        $method,
        $url,
        $query,
        $body,
        $nonce,
        $timestamp,
        $username=$username
    );

    $rsa->setSignatureMode(RSA::SIGNATURE_PSS);
    $rsa->setHash('sha512');
    $rsa->setMGFHash('sha512');
    $emlen = (int) ceil(($rsa->getSize() - 1) / 8.0);
    $salt_length = $emlen - 64 - 2;
    $rsa->setSaltLength($salt_length);
    return base64_encode($rsa->sign($base));
  }

  function verify_rsa_sha512_signature(
      $rsa,
      $signature,
      $method,
      $url,
      $query,
      $username,
      $body,
      $nonce,
      $timestamp) {
    $base = $this->build_string(
        $method,
        $url,
        $query,
        $body,
        $nonce,
        $timestamp,
        $username=$username
    );
    $rsa->setSignatureMode(RSA::SIGNATURE_PSS);
    $rsa->setHash('sha512');
    $rsa->setMGFHash('sha512');
    $emlen = (int) ceil(($rsa->getSize() - 1) / 8.0);
    $salt_length = $emlen - 64 - 2;
    $rsa->setSaltLength($salt_length);
    return $rsa->verify($base, base64_decode($signature));
  }

  private function quote($v) {
    return implode('/', array_map('rawurlencode', explode('/', $v)));
  }

  private function build_string(
      $method,
      $url,
      $query,
      $body,
      $nonce,
      $timestamp,
      $username=false,
      $token_id=false) {

    $buffer = '';
    $buffer .= strtoupper($method) . '&' . $this->quote($url) . '&';

    $q = $query;
    $q['auth_signature_method'] = 'RSA-SHA512';
    $q['auth_timestamp'] = (string) $timestamp;
    if ($username !== false) {
      $q['auth_username'] = $username;
    } else if ($token_id !== false) {
      $q['auth_token_id'] = $token_id;
    }
    $q['auth_version'] = '1.0';
    $q['auth_nonce'] = $nonce;
    $q2 = array();
    foreach($q as $k => $v) {
      $q2[$this->quote($k)] = $this->quote($v);
    }
    $q = $q2;
    asort($q);
    ksort($q);

    $b = array();
    foreach($q as $k => $v) {
      array_push($b, $k . '=' . $v);
    }
    $buffer .= $this->quote(implode('&', $b));

    $buffer .= '&' . $this->quote($body);

    return $this->quote($buffer);
  }

}
?>
