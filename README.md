# Chronicle Platform PHP Library

> Lifetime tracking for physical artefacts.

Common PHP code for the Chronicle platform including clients.

## Requirements

* Required
    * [PHP](https://www.php.new/) (>7)

## Release History

* 0.0.1
    * Work in progress

## Meta

© 2018 Horizon Digital Economy Research
[www.horizon.ac.uk](https://www.horizon.ac.uk)

[https://bitbucket.org/horizon-dev/lib-chronicle-php](
https://bitbucket.org/horizon-dev/lib-chronicle-php)

## Contributing

1. Fork it (<https://bitbucket.org/horizon-dev/lib-chronicle-php>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
